[DEFAULT]
# openweathermap_api_key
app_id = %(openweathermap_api_key)s
influxdb_host = influxdb
influxdb_port = 8086
influxdb_database = weather
units = metric
lang = en

[Arkham]
city_id = 5750162
lat = 44.94
lon = -123.04
