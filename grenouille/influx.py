# coding: utf-8

import logging
from os import environ

from influxdb import InfluxDBClient
from influxdb import SeriesHelper
from influxdb.exceptions import InfluxDBClientError, InfluxDBServerError


class WeatherSeries(SeriesHelper):
    """Instantiate SeriesHelper to write points to the backend."""
    # src: https://influxdb-python.readthedocs.io/en/latest/examples.html#tutorials-serieshelper

    class Meta:
        """Meta class stores time series helper configuration."""
        # The client should be an instance of InfluxDBClient.
        #client = myclient

        # The series name must be a string. Add dependent fields/tags
        # in curly brackets.
        series_name = 'owm'
        # Defines all the fields in this time series.
        fields = ['temp', 'temp_max', 'temp_min', 'pressure',
                  'humidity', 'uvi', 'wind_speed', 'wind_direction',
                  'wind_card_direction', 'sea_level', 'grnd_level',
                  'rain_3h', 'rain_1h', 'cloudiness', 'weather', 'weather_id']
        # Defines all the tags for the series.
        tags = ['location', 'type']

        # Defines the number of data points to store prior to writing
        # on the wire.
        bulk_size = 5
        # autocommit must be set to True when using bulk_size
        autocommit = True


def set_influxclient(config):
    """Set InfluxDB con. client"""
    default = config['DEFAULT']
    influxdb_password = environ.get('INFLUXDB_ADMIN_PASSWORD')
    influxdb_password = default.get('influxdb_password', influxdb_password)
    influxdb_user = environ.get('INFLUXDB_ADMIN_USER')
    influxdb_user = default.get('influxdb_user', influxdb_user)
    logging.debug('user: "%s", password: "%s"',
                  influxdb_user, influxdb_password)
    if environ.get('INFLUXDB_ADMIN_PASSWORD'):
        logging.info('Got influxdb admin passwd from INFLUXDB_ADMIN_PASSWORD')
    myclient = InfluxDBClient(config['DEFAULT']['influxdb_host'],
                              config['DEFAULT']['influxdb_port'],
                              influxdb_user,
                              influxdb_password,
                              config['DEFAULT']['influxdb_database'])
    WeatherSeries.Meta.client = myclient

def write(payload):
    try:
        WeatherSeries(**payload)
        logging.debug(payload)
    except InfluxDBClientError as err:
        logging.error(err)
        logging.info(payload)
        logging.debug(WeatherSeries._json_body_())
    except InfluxDBServerError as err:
        logging.error(err)
        return False
    return True

def commit():
    try:
        WeatherSeries.commit()
        logging.debug(WeatherSeries._json_body_())
    except (InfluxDBClientError, InfluxDBServerError) as err:
        logging.error(err)
        logging.info(WeatherSeries._json_body_())

# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab fileencoding=utf8
