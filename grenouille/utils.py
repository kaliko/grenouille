# coding: utf-8

import logging

from bisect import bisect
from datetime import datetime

from .influx import WeatherSeries

# Cardinal point in degrees
BREAKPOINTS = [0, 22.5, 45, 67.5, 90, 112.5, 135, 157.5, 180, 202.5, 225,
               247.5, 270, 292.5, 315, 337.5, 360]
# Set break points
BREAKPOINTS = [d-(22.5/2) for d in BREAKPOINTS]

CARDIANAL_POINTS = ['N', 'N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S',
                    'SSO', 'SO', 'OSO', 'O', 'ONO', 'NO', 'NNO', 'N']


def wind(wind_direction):
    """Give wind_direction in Degrees
    """
    # https://docs.python.org/3.7/library/bisect.html#other-examples
    i = bisect(BREAKPOINTS, wind_direction)
    return CARDIANAL_POINTS[i]


def extract_wind(data):
    """OWM wind format to Influx Schema
    input : {speed': 1.65, 'deg': 292.267}
    output: {'wind_speed': <float>, 'wind_direction': <float>,
             'wind_card_direction': <str>}
    """
    wind_dir = int(data['deg'])
    payload = ({'wind_speed': data['speed'],
                'wind_direction': wind_dir})
    payload.update({'wind_card_direction': wind(wind_dir)})
    return payload


def extract_rain(data):
    """rain data is not alway present"""
    if 'rain' not in data:
        return {}
    rain = data['rain']
    pld = {}
    if '3h' in rain:
        pld.update({'rain_3h': rain['3h']})
    if '1h' in rain:
        pld.update({'rain_1h': rain['1h']})
    return pld


def ts_to_iso(ts):
    return datetime.utcfromtimestamp(ts).isoformat()


def collapse_type(payload):
    """Ensure types remains consistant"""
    fieldtypes = {
        'temp': float,
        'temp_max': float,
        'temp_min': float,
        'pressure': int,
        'humidity': int,
        'uvi': float,
        'wind_speed': float,
        'wind_direction': int,
        'wind_card_direction': str,
        'sea_level': float,
        'grnd_level': float,
        'rain_3h': float,
        'rain_1h': float,
        'cloudiness': int,
        'weather': str,
        'weather_id': str
    }
    for key, val in payload.items():
        if key == 'time':
            continue
        if key in WeatherSeries.Meta.tags:
            continue
        if key not in WeatherSeries.Meta.fields:
            logging.warning('Unknown field "%s"', key)
            continue
        fieldtype = fieldtypes.get(key)
        if not isinstance(val, fieldtype):
            try:
                payload.update({key: fieldtype(val)})
            except (TypeError, ValueError):
                logging.warning('Cannot set value type for %s:"%s"→%s', key, val, fieldtype)


# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab fileencoding=utf8
