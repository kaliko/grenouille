# coding: utf-8

import logging
import sys
# import json

from configparser import ConfigParser, BasicInterpolation
from os import environ

import requests

from .influx import set_influxclient, write, commit
from .utils import extract_wind, extract_rain, ts_to_iso, collapse_type


class OpenWeatherMap:
    uri = 'http://api.openweathermap.org/data/2.5/'
    endpoints = {
        'weather':  'id={city_id}&appid={app_id}&units={units}&lang={lang}',
        'forecast': 'id={city_id}&appid={app_id}&units={units}&lang={lang}',
        'uvi':      'lat={lon}&lon={lat}&appid={app_id}',
    }

    def __init__(self, config):
        self.config = config
        set_influxclient(config)
        self.payload = None
        self.location = None
        for city in config.sections():
            cfg = config[city]
            self.location = city
            for endpoint in self.endpoints:
                self._get(endpoint, cfg)
        commit()

    def _get(self, endpoint, cfg):
        logging.debug('running %s/%s', cfg, endpoint)
        query_str = self.endpoints.get(endpoint)
        uri = '{}{}?{}'.format(self.uri, endpoint, query_str)
        url = uri.format(**cfg)
        try:
            logging.debug('GET "%s"', url)
            #TODO: reuse connection?
            get = requests.get(url)
            if get.status_code != 200:
                logging.warning('GET /%s returned "%s:%s"',
                                endpoint, get.status_code, get.reason)
                return False
            data = get.json()
            # with open('tests/{}.json'.format(endpoint)) as jsondata:
            #     data = json.load(jsondata)
        except ConnectionError as err:
            logging.error('Connection Error: "%s"', err)
            return False
        except requests.exceptions.RequestException as err:
            logging.error('requests error: "%s"', err)
            return False
        self._write_series(endpoint, data)
        return True

    def _write_payloads(self, payload, data):
        payload.update({'cloudiness': data['clouds']['all']})
        payload.update(extract_wind(data['wind']))
        payload.update(extract_rain(data))
        weather_descrition = [w['description'] for w in data['weather']]
        payload.update({'weather': '/'.join(weather_descrition)})
        payload.update({'weather_id': '/'.join([str(w['id']) for w in data['weather']])})
        collapse_type(payload)
        if not write(payload):
            sys.exit(1)

    def _write_series(self, endpoint, data):
        if endpoint == 'forecast':
            for point in data['list']:
                payload = {'location': self.location,
                           'type': 'forecast'}
                payload.update({'time': ts_to_iso(point['dt'])})
                levels = point['main']
                levels.pop('temp_kf')
                payload.update(levels)
                self._write_payloads(payload, point)
        if endpoint == 'weather':
            payload = {'location': self.location, 'type': 'measurement'}
            payload.update({'time': ts_to_iso(data['dt'])})
            # temp(current, min, max) pressure, humidity
            levels = data['main']
            payload.update(levels)
            self._write_payloads(payload, data)
        if endpoint == 'uvi':
            payload = {'location': self.location, 'type': 'measurement'}
            payload.update({'uvi': data['value']})
            payload.update({'time': data['date_iso']})
            if not write(payload):
                sys.exit(1)

def main():
    from os.path import abspath, dirname, join
    if environ.get('DEBUG', False):
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    config = ConfigParser(interpolation=BasicInterpolation())
    config_file = join(abspath(dirname(sys.argv[0])), 'config.cfg')
    config.read(config_file)
    OpenWeatherMap(config)


# Script starts here
if __name__ == '__main__':
    main()

# VIM MODLINE
# vim: ai ts=4 sw=4 sts=4 expandtab fileencoding=utf8
